import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrormessageService {
  errorMessage: string | null = null;

  constructor() { }

  generateErrorMessageHtml(errorMessage: string | string[]): string {
    if (
      !errorMessage ||
      (typeof errorMessage === "string" && errorMessage.trim() === "")
    ) {
      return ""; // Return an empty string if errorMessage is empty or not a valid string
    }

    if (typeof errorMessage === "string") {
      errorMessage = errorMessage.split("\n");
    }

    let html: string = "<ul>";
    for (const line of errorMessage) {
      html += `<li>${line}</li>`;
    }
    html += "</ul>";
    return html;
  }
}
